function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'item missing required property ' + propertyName
    return property;
}
export default {
    async authenticate(context, payload) {
        const token = window.btoa(payload.login.username + ":" + payload.login.password);
        const response = await this.$axios.get('/user/detail/username/' + payload.login.username, {headers: {'Authorization': `Basic ${token}`}});
        const loggedUser = {
            authenticationToken: token,
            username: validateProperty(response,"data"),
        }
        context.commit('setLoggedUser', loggedUser);
    }
};
