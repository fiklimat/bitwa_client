function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'item missing required property ' + propertyName
    return property;
}
function getValidatedUnprocessedOrder (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        surname: validateProperty(data, 'surname'),
        street: validateProperty(data, 'street'),
        number: validateProperty(data, 'number'),
        town: validateProperty(data, 'town'),
        post_code: validateProperty(data, 'post_code'),
        email: validateProperty(data, 'email'),
        phone_number: validateProperty(data, 'phone_number'),
        total_price: validateProperty(data, 'total_price'),
        created: validateProperty(data, 'created'),
        unprocessed_order_items: data['unprocessed_order_item'],
        display: data['id'] + ', ' + data['name'] + ', ' + data['surname'] + ', ' + data['email'] + ', ' + data['total_price'] + ' Kč, ' + data['created']
    };
}
function getValidatedProcessedOrder (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        surname: validateProperty(data, 'surname'),
        street: validateProperty(data, 'street'),
        number: validateProperty(data, 'number'),
        town: validateProperty(data, 'town'),
        post_code: validateProperty(data, 'post_code'),
        email: validateProperty(data, 'email'),
        phone_number: validateProperty(data, 'phone_number'),
        total_price: validateProperty(data, 'total_price'),
        processed: validateProperty(data, 'processed'),
        processed_order_items: data['processed_order_item'],
        display: data['id'] + ', ' + data['name'] + ', ' + data['surname'] + ', ' + data['email'] + ', ' + data['total_price'] + ' Kč, ' + data['processed']
    };
}
export default {
    async loadUnprocessedOrders(context) {
        const response = await this.$axios.$get('/orders/unprocessed/all', {headers: {'Authorization': `Basic ${this.state.user.loggedUser.authenticationToken}`}});
        const orders = []
        for (const key in response) {
            const order = getValidatedUnprocessedOrder(response[key]);
            orders.push(order);
        }
        context.commit('setUnprocessedOrders', orders);
        return orders;
    },
    async processOrder(context, payload) {
        await this.$axios.$get('orders/process/' + payload, {headers: {'Authorization': `Basic ${this.state.user.loggedUser.authenticationToken}`}});

    },
    async loadProcessedOrders(context) {
        const response = await this.$axios.$get('/orders/processed/all', {headers: {'Authorization': `Basic ${this.state.user.loggedUser.authenticationToken}`}});
        const orders = []
        for (const key in response) {
            const order = getValidatedProcessedOrder(response[key]);
            orders.push(order);
        }
        context.commit('setProcessedOrders', orders);
        return orders;
    }
};
