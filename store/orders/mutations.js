export default {
    setUnprocessedOrders(state, payload) {
        state.unprocessed_orders = payload;
    },
    setProcessedOrders(state, payload) {
        state.processed_orders = payload;
    }
};
