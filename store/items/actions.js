function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'item missing required property ' + propertyName
    return property;
}
function getValidatedItemShort (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        price: validateProperty(data, 'price'),
        quantity: validateProperty(data, 'quantity'),
        subcategories: data['subcategories'],
        fits_filter: true
    };
}
function getValidatedItemLong (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        short_description: validateProperty(data, 'short_description'),
        long_description: validateProperty(data, 'long_description'),
        price: validateProperty(data, 'price'),
        quantity: validateProperty(data, 'quantity'),
        subcategories: data['subcategories'],
    };
}
export default {
    async loadItems(context) {
        const response = await this.$axios.$get('/item/all');
        const items = []
        for (const key in response) {
            const item = getValidatedItemShort(response[key]);
            items.push(item);
        }
        context.commit('setItems', items);
        return items;
    },
    async loadItemsForCategory(context, payload) {
        const response = await this.$axios.$get('/item/category/' + payload);
        const items = []
        for (const key in response) {
            const item = getValidatedItemShort(response[key]);
            items.push(item);
        }
        context.commit('setItems', items);
        return items;
    },
    async loadSelectedItem(context, payload) {
        const response = await this.$axios.$get('item/detail/' + payload);
        const item = getValidatedItemLong(response);
        context.commit('setDetailedItem', item);
        return item;
    }
};
