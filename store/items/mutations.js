export default {
    setItems(state, payload) {
        state.items = payload;
    },
    setDetailedItem(state, payload) {
        state.detailed_item = payload;
    }
};
