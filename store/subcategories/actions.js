function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'item missing required property ' + propertyName
    return property;
}
function getValidatedSubcategory (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        category_id: validateProperty(data, 'category_id'),
        category_name: validateProperty(data, 'category_name'),
        value_checked: false,
        values: []
    };
}
export default {
    async loadSubcategories(context, payload) {
        const response = await this.$axios.$get('/subcategory/category/' + payload);
        const subcategories = []
        for (const key in response) {
            const subcategory = getValidatedSubcategory(response[key]);
            subcategories.push(subcategory);
        }
        context.commit('setSubcategories', subcategories);
        return subcategories;
    }
};
