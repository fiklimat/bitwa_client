export default {
    setDates(state, payload) {
        state.dates = payload;
    },
    setSales(state, payload) {
        state.sales = payload;
    }
};
