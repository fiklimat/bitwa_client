export default {
    async getSaleDates(context) {
        const response = await this.$axios.$get('/sales/dates/all', {headers: {'Authorization': `Basic ${this.state.user.loggedUser.authenticationToken}`}});
        const dates = [];
        for (const key in response) {
            const date = {
                date: response[key].date,
                display: response[key].date.slice(0,6)
            }
            dates.push(date);
        }
        context.commit('setDates',dates);
        return dates;
    },
    async getSales (context, payload) {
        const dates = [];
        for (const key in payload) {
            const date = payload[key].date;
            dates.push(date);
        }
        const response = await this.$axios.post('/sales/filtered/date', dates, {headers: {'Authorization': `Basic ${this.state.user.loggedUser.authenticationToken}`}});
        context.commit('setSales',response.data);
        const days = [];
        const numbers =[];
        for (const key in response.data) {
            numbers.push(response.data[key].number);
            days.push(response.data[key].date.slice(0, 6))
        }
        return {days: days, numbers: numbers};
    }
};
