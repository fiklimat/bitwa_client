export default {
     addItemToCart(context, payload) {
         const item = {
             id: payload.id,
             name: payload.name,
             price: payload.price,
             quantity: payload.quantity,
             selected_quantity: 1
         }
         context.commit('addToCart', item);
    },
    removeItemFromCart (context, payload) {
         context.commit('removeFromCart', payload);
    },
    emptyCart (context) {
      context.commit('emptyCart')
    },
    async finishOrder (context, payload) {
        const response = await this.$axios.post('/order/create', payload);
        return response.data;
    }
};
