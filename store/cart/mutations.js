export default {
    addToCart(state, payload) {
        if (state.store.cart.find(c => c.id === payload.id)) {
            if (state.store.cart.find(c => c.id === payload.id && c.selected_quantity < c.quantity))
                state.store.cart.find(c => c.id === payload.id).selected_quantity += 1;
        }
        else
            state.store.cart.push(payload);
        state.store.total_price += payload.price;
    },
    removeFromCart(state, payload) {
        state.store.total_price -= state.store.cart.find(c => c.id === payload).price;
        if (state.store.cart.find(c => c.id === payload && c.selected_quantity > 1))
            state.store.cart.find(c => c.id === payload).selected_quantity -= 1;
        else
            state.store.cart = state.store.cart.filter(c => c.id !== payload);
    },
    emptyCart(state) {
        state.store.total_price = 0;
        state.store.cart = [];
    }
};
